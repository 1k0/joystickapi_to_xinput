# README #

### About ###
Minimal Joysickapi to XInput adapter. Written to work with X-Wing Alliance.
### Build ###
Use build.bat from a development command prompt to build winnn.dll
### Dependencies ###
XInput is required to be present
### Usage ###
-Copy winnn.dll to .exe directory
-Patch .exe to load winnn.dll instead of winmm.dll
