#include "joystickapi_to_xinput.h"

#include <xinput.h>
#include <stdint.h>

typedef uint32_t u32;
typedef int32_t i32;

//QUIRKS : 
/*
-Report 16 (max) devices map first xinput device to all 16 slots so no matter
which controller the user picks in the launcher.
-Report only 2 axes and a POV hat, handle the rest as buttons
-Use right analog as pov, dpad as buttons
*/

static const char gProductName[] = "XInput-Joystickapi adapter";
static const char gRegKey[] = "DINPUT.DLL";
static i32 gXInputControllerID = -1;
static u32 gPovTable[16] = {
    65535,      //0000-none
    0,          //0001-up
    18000,      //0010-down
    65535,      //0011-inv
    27000,      //0100-left
    31500,      //0101-left up
    22500,      //0110-left down
    65535,      //0111-inv
    9000,       //1000-right
    4500,       //1001-right up
    13500,      //1010-right down
    65535,      //1011-inv
    65535,      //1100-inv
    65535,      //1101-inv
    65535,      //1110-inv
    65535,      //1111-inv
};

static u32 bitCount(u32 v)
{
    u32 c;
    v = v - ((v >> 1) & 0x55555555);
    v = (v & 0x33333333) + ((v >> 2) & 0x33333333);
    c = ((v + (v >> 4) & 0xF0F0F0F) * 0x1010101) >> 24;
    return c;
}

BOOL WINAPI DllMain(HANDLE hModule, DWORD fdwReason, LPVOID lpReserved)
{
    switch (fdwReason)
    {
        case DLL_PROCESS_ATTACH : 
        {
            XINPUT_STATE state = {};
            for (u32 i = 0; i < XUSER_MAX_COUNT; ++i)
            {
                if (XInputGetState(i, &state) == ERROR_SUCCESS)
                {
                    gXInputControllerID = i;
                    break;
                }
            }
        }
        break;

        case DLL_PROCESS_DETACH : 
        {
            gXInputControllerID = -1;
        }
        break;
    }
    return TRUE;
}

extern "C"
{
    UINT WINAPI HACK_joyGetNumDevs()
    {
        return XUSER_MAX_COUNT;
    }

    MMRESULT WINAPI HACK_joyGetDevCapsA(UINT id, JOYCAPS* caps, UINT sizeofCaps)
    {
        (*caps) = {};
        //NOTE - WARN : special case handle it above range check. (Watch out for int uint mismatch)
        if (id == -1)
        {
            memcpy(caps->szRegKey, gRegKey, sizeof(gRegKey));
            return JOYERR_NOERROR;
        }

        if (id >= XUSER_MAX_COUNT)
        {
            return MMSYSERR_NODRIVER;
        }

        //Data captured from a real XInput gamepad, should be OK
        caps->wMid = 1103;
        caps->wPid = 45862;
        memcpy(caps->szRegKey, gRegKey, sizeof(gRegKey));
        memcpy(caps->szPname, gProductName, sizeof(gProductName));
        caps->szOEMVxD[0] = '\0';

        //Axe, hat caps (joystick is assumed to have at least 2 axis, so no need to report them?)
        caps->wCaps = (JOYCAPS_HASPOV | JOYCAPS_POV4DIR);
        //Analog ranges
        caps->wXmin = 0;
        caps->wXmax = 65535;
        caps->wYmin = 0;
        caps->wYmax = 65535;
        caps->wZmin = 0;
        caps->wZmax = 65535;
        caps->wRmin = 0;
        caps->wRmax = 65535;
        caps->wUmin = 0;
        caps->wUmax = 65535;
        caps->wVmin = 0;
        caps->wVmax = 65535;
        //Polling period
        caps->wPeriodMin = 10;
        caps->wPeriodMax = 1000;
        //Axe count
        caps->wNumAxes = 5;
        caps->wMaxAxes = 6;
        //Button count
        caps->wNumButtons = 16;
        caps->wMaxButtons = 32;

        return JOYERR_NOERROR;
    }

    MMRESULT WINAPI HACK_joyGetPosEx(UINT id, JOYINFOEX* info)
    {
        (*info) = {};

        if (id >= XUSER_MAX_COUNT)
        {
            return MMSYSERR_NODRIVER;
        }

        if (gXInputControllerID != -1)
        {
            XINPUT_STATE state = {};
            if (XInputGetState(gXInputControllerID, &state) != ERROR_SUCCESS)
            {
                gXInputControllerID = -1;
            }

            u32 buttons = state.Gamepad.wButtons;
            buttons |= ((state.Gamepad.bLeftTrigger != 0) << 10);
            buttons |= ((state.Gamepad.bRightTrigger != 0) << 11);
            
            info->dwButtons = buttons;
            info->dwButtonNumber = bitCount(info->dwButtons);

            u32 pov = 0;
            pov |= (state.Gamepad.sThumbRY > 16384) ? (1 << 0) : (0);
            pov |= (state.Gamepad.sThumbRY < -16384) ? (1 << 1) : (0);
            pov |= (state.Gamepad.sThumbRX < -16384) ? (1 << 2) : (0);
            pov |= (state.Gamepad.sThumbRX > 16384) ? (1 << 3) : (0);
            info->dwPOV = gPovTable[pov];

            info->dwXpos = ((i32) state.Gamepad.sThumbLX + 32768);
            info->dwYpos = ((i32) state.Gamepad.sThumbLY + 32768);
        }

        return JOYERR_NOERROR;
    }
}